import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertManager } from '../helpers/alert';
import {environment} from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-my-ads-details',
  templateUrl: './my-ads-details.page.html',
  styleUrls: ['./my-ads-details.page.scss'],
})
export class MyAdsDetailsPage implements OnInit {

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode : DarkModeService
  ) { }

  ad;
  httpOptions;
  quantity;
  discount;
  isLogOut: boolean
  isDeleted: boolean
  isDiscount: boolean
  publicUrl;

  ngOnInit() {

    this.darkMode.setDarkModeAllPages();
    this.publicUrl = environment.publicUrl
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
  }

  ionViewWillEnter() {
    this.http.get(environment.apiBaseUrl + '/ad/' + localStorage.getItem('ad'), this.httpOptions)
        .subscribe( response => {
          console.log(response);
          this.ad = response['ad'];
        }, error => {
          console.log(error);
    });
  }

  delete() {
    this.isDeleted = true;
    this.http.post(environment.apiBaseUrl + '/ads/' + localStorage.getItem('ad'),null, this.httpOptions)
        .subscribe( response => {
          this.alertManager.showAlert("Success!",response['message'],'my-ads')
          console.log(response);
        }, error => {
          console.log(error);
          this.isDeleted = false;
          this.alertManager.showAlert("Error!", error.statusText, "")
    });
  }

  logout() {
    this.isLogOut = true;
    this.http.post(environment.apiBaseUrl + "/logout",null, this.httpOptions).subscribe( response => {
      this.alertManager.showAlert("Success!", response['message'], 'home')
    }, error => {
      this.isLogOut = false;
      this.alertManager.showAlert("Error!", error.statusText, "")
    })
  }

  applyDiscount() {
    if(this.discount === undefined) return;
    this.isDiscount = true;

    this.http.post(environment.apiBaseUrl + "/ads/apply-discount",
    {
      ad_id: localStorage.getItem('ad'),
      discount: this.discount
    }, this.httpOptions).subscribe( response => {
      this.isDiscount = false;
      this.alertManager.showAlert("Success!", response['message'], 'my-ads')
    }, error => {
      this.isDiscount = false;
      console.log(error)
      this.alertManager.showAlert("Error!", error.error.message, "")
    })
  }

  removeDiscount() {
    this.isDiscount = true;
    this.http.post(environment.apiBaseUrl + "/ads/remove-discount",
    {
      ad_id: localStorage.getItem('ad')
    }, this.httpOptions).subscribe( response => {
      this.isDiscount = false;
      this.alertManager.showAlert("Success!", response['message'], 'my-ads')
    }, error => {
      this.isDiscount = false;
      this.alertManager.showAlert("Error!", error.error.message, "")
    })
  }
}
