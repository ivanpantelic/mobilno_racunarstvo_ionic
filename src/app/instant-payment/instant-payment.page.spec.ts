import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantPaymentPage } from './instant-payment.page';

describe('InstantPaymentPage', () => {
  let component: InstantPaymentPage;
  let fixture: ComponentFixture<InstantPaymentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantPaymentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantPaymentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
