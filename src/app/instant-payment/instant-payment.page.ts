import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertManager } from '../helpers/alert';
import { DarkModeService } from '../services/dark-mode.service';
import { ToastController, LoadingController, Platform } from '@ionic/angular';
import jsQR from 'jsqr';
import { getClosureSafeProperty } from '@angular/core/src/util/property';
import { DomSanitizer } from '@angular/platform-browser';


declare var Stripe: any;

@Component({
  selector: 'app-instant-payment',
  templateUrl: './instant-payment.page.html',
  styleUrls: ['./instant-payment.page.scss'],
})


export class InstantPaymentPage implements OnInit {

  @ViewChild('video') video: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('fileinput') fileinput: ElementRef;

  canvasElement: any; videoElement: any; canvasContext: any; loading: HTMLIonLoadingElement = null;
  scanActive = false; scanResult = null;
  takePaymentResult: string;
  httpOptions;
  qrCode; publicUrl; url;
  showData: boolean = false; ipsScan: boolean = false; ipsShow: boolean = true;
  receiver; sender; code; purpose; account_number; model; total_amount; order_id;transaction_id;
  data: any;
  isFinished: boolean;
  customer_id: string;
  transaction_status: string = "Success";
  base64: string;

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode: DarkModeService,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private plt: Platform
  ) {
    const isInStandaloneMode = () =>
      'standalone' in window.navigator && window.navigator['standalone'];
    if (this.plt.is('ios') && isInStandaloneMode()) {
      console.log('I am a an iOS PWA!');
    }
  }

  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.customer_id = localStorage.getItem('customerId');
    this.transaction_id = localStorage.getItem('transactionId');
    this.order_id = localStorage.getItem('orderId');
    this.publicUrl = environment.publicUrl;
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
    this.qrCode = localStorage.getItem("qrURL");
  }


  finishPayment() {
    this.isFinished = true;
    this.http.post(environment.apiBaseUrl + '/charges', {
      address: this.data.adresa,
      note: this.data.napomena,
      amount: this.data.iznos,
      customer: this.customer_id,
      transaction_id: this.transaction_id,
      status: this.transaction_status,
      order_id: this.order_id
    }, this.httpOptions)
      .subscribe(response => {
        console.log(response);
        this.alertManager.showAlert("Success!", response['message'], 'tabs/my-profile')
        this.isFinished = false;
      }, error => {
        this.isFinished = false;
        console.log(error);
        if (error.status == 422) {
          this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
        } else {
          this.alertManager.showAlert("Error!", error['error'].message, "")
        }
      });
  }

  // updateTransaction() {
  //   this.http.post(environment.apiBaseUrl + '/transaction-update', {
  //     transaction_id: "2",
  //     status: this.transaction_status
  //   }, this.httpOptions)
  //     .subscribe(response => {
  //       console.log(response);
  //     }, error => {
  //       console.log(error);
  //       if (error.status == 422) {
  //         this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
  //       } else {
  //         this.alertManager.showAlert("Error!", error['error'].message, "")
  //       }
  //     });
  // }

  // finishPayment() {
  //   this.isFinished = true;
  //   this.http.post(environment.apiBaseUrl + '/order', {
  //     address: this.data.adresa,
  //     note: this.data.napomena,
  //     amount: this.data.iznos,
  //     receiver: this.data.primalac,
  //   }, this.httpOptions)
  //     .subscribe(response => {
  //       console.log(response);
  //       this.alertManager.showAlert("Success!", response['message'], 'tabs/my-profile')
  //       this.isFinished = false;
  //     }, error => {
  //       this.isFinished = false;
  //       console.log(error);
  //       if (error.status == 422) {
  //         this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
  //       } else {
  //         this.alertManager.showAlert("Error!", error['error'].message, "")
  //       }
  //     });
  // }

  async downloadQR() {
    this.url = this.publicUrl + '/' + this.qrCode;
    this.http.post(environment.apiBaseUrl + "/getQR", {
      image_url: this.qrCode,
    },this.httpOptions).subscribe( response => {
      this.base64 = response['base64'];
    }, error => {
      console.log(error);
      this.alertManager.showAlert("Error!", error.statusText, "")
    })
    var img = 'data:application/octet-stream;base64,'+ this.base64;
    var a = document.createElement("a"); 
    a.href = img;
    a.download = "qr.png"; 
    a.click();
  }


  toDataURL(url) {
    return fetch(url, { mode: 'no-cors' }).then((response) => {
      return response.blob();
    }).then(blob => {
      return URL.createObjectURL(blob);
    });
  }

  isIPShow() {
    this.ipsShow = true;
    this.ipsScan = false;
    this.showData = false;
  }

  isIPSScan() {
    this.ipsScan = true;
    this.ipsShow = false;
    this.showData = false;
    this.scanResult = false;
  }

  reset() {
    this.scanResult = null;
  }

  stopScan() {
    this.scanActive = false;
  }

  repeatScan() {
    this.scanResult = null;
    this.startScan();
  }

  async showQrToast() {
    const toast = await this.toastCtrl.create({
      message: `Open ${this.scanResult}?`,
      position: 'top',
      buttons: [
        {
          text: 'Open',
          handler: () => {
            window.open(this.scanResult, '_system', 'location=yes');
          }
        }
      ]
    });
    toast.present();
  }

  async startScan() {
    this.canvasElement = this.canvas.nativeElement;
    this.canvasContext = this.canvasElement.getContext('2d');
    this.videoElement = this.video.nativeElement;
    const stream = await navigator.mediaDevices.getUserMedia({
      video: { facingMode: 'environment' }
    });

    this.videoElement.srcObject = stream;
    this.videoElement.setAttribute('playsinline', true);

    this.loading = await this.loadingCtrl.create({});
    await this.loading.present();

    this.videoElement.play();
    requestAnimationFrame(this.scan.bind(this));
  }

  async scan() {
    if (this.videoElement.readyState === this.videoElement.HAVE_ENOUGH_DATA) {
      if (this.loading) {
        await this.loading.dismiss();
        this.loading = null;
        this.scanActive = true;
      }

      this.canvasElement.height = this.videoElement.videoHeight;
      this.canvasElement.width = this.videoElement.videoWidth;

      this.canvasContext.drawImage(
        this.videoElement,
        0,
        0,
        this.canvasElement.width,
        this.canvasElement.height
      );
      const imageData = this.canvasContext.getImageData(
        0,
        0,
        this.canvasElement.width,
        this.canvasElement.height
      );
      const code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: 'dontInvert'
      });

      if (code) {
        this.scanActive = false;
        this.scanResult = code.data;
        this.data = JSON.parse(code.data);
        this.alertManager.showAlert("Error!", "Can not read a QR code!", "")
        this.populateFromQR();
        this.ipsScan = false;
        this.showData = true;
      } else {
        if (this.scanActive) {
          requestAnimationFrame(this.scan.bind(this));
        }
      }
    } else {
      requestAnimationFrame(this.scan.bind(this));
    }
  }

  captureImage() {
    this.canvasElement = this.canvas.nativeElement;
    this.canvasContext = this.canvasElement.getContext('2d');
    this.videoElement = this.video.nativeElement;
    this.fileinput.nativeElement.click();
  }

  handleFile(files: FileList) {
    const file = files.item(0);

    var img = new Image();
    img.onload = () => {
      this.canvasContext.drawImage(img, 0, 0, this.canvasElement.width, this.canvasElement.height);
      const imageData = this.canvasContext.getImageData(
        0,
        0,
        this.canvasElement.width,
        this.canvasElement.height
      );
      const code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: 'dontInvert'
      });

      if (code) {
        this.scanResult = code.data;
        this.data = JSON.parse(code.data);
        this.populateFromQR();
        this.ipsScan = false;
        this.showData = true;
      }
    };
    img.src = URL.createObjectURL(file);
  }

  populateFromQR() {
    this.receiver = this.data.primalac;
    this.sender = this.data.platilac;
    this.code = this.data.sifra_placanja;
    this.purpose = this.data.svrha_uplate;
    this.account_number = this.data.u_korist_racuna;
    this.model = this.data.model_poziv;
    this.total_amount = this.data.iznos + " " + this.data.valuta;
  }

}
