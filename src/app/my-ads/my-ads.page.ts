import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';
import {environment} from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-my-ads',
  templateUrl: './my-ads.page.html',
  styleUrls: ['./my-ads.page.scss'],
})
export class MyAdsPage implements OnInit {

  httpOptions;
  ads = [];
  publicUrl;

  constructor(
    private http: HttpClient,
    private router: Router,
    private darkMode : DarkModeService
  ) { }
  
  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.publicUrl = environment.publicUrl
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
  }

  ionViewWillEnter() {
    this.http.get(environment.apiBaseUrl + '/ads', this.httpOptions)
        .subscribe( response => {
          console.log(response);
          this.ads = response['orders'];
        }, error => {
          console.log(error);
    });
  }
  details(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    
    localStorage.setItem('ad', value);
    this.router.navigate(['/my-ads-details']);
  }

}
