import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAdsPage } from './my-ads.page';

describe('MyAdsPage', () => {
  let component: MyAdsPage;
  let fixture: ComponentFixture<MyAdsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAdsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAdsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
