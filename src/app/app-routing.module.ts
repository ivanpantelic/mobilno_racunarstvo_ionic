import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'tabs', loadChildren: './main-screen/main-screen.module#MainScreenPageModule' },
  { path: 'splash-screen', loadChildren: './splash-screen/splash-screen.module#SplashScreenPageModule' },
  { path: 'my-cart', loadChildren: './my-cart/my-cart.module#MyCartPageModule' },
  { path: 'finish-order', loadChildren: './finish-order/finish-order.module#FinishOrderPageModule' },
  { path: 'order-history', loadChildren: './order-history/order-history.module#OrderHistoryPageModule' },
  { path: 'ad-details', loadChildren: './ad-details/ad-details.module#AdDetailsPageModule' },
  { path: 'my-ads', loadChildren: './my-ads/my-ads.module#MyAdsPageModule' },
  { path: 'my-ads-details', loadChildren: './my-ads-details/my-ads-details.module#MyAdsDetailsPageModule' },
  { path: 'camera', loadChildren: './camera/camera.module#CameraPageModule' },
  { path: 'upload-photo', loadChildren: './upload-photo/upload-photo.module#UploadPhotoPageModule' },
  { path: 'instant-payment', loadChildren: './instant-payment/instant-payment.module#InstantPaymentPageModule' },





];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
