import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {
    private counter = 0;
  transform(items: any[], searchText: string): any[] {
      this.counter++;
      console.log('Filter pipe executed count ' + this.counter); 
    if(!items || !searchText) return items;
    
    return items.filter( it => {
      return it.toString().toLowerCase().indexOf(searchText.toString().toLowerCase()) !== -1;
    });
   }
}