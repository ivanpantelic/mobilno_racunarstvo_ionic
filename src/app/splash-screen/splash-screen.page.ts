import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';
import { AlertManager } from '../helpers/alert';
import { environment } from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.page.html',
  styleUrls: ['./splash-screen.page.scss'],
})
export class SplashScreenPage implements OnInit {

  httpOptions;
  ads = [];
  searchText: String
  match: boolean = false;
  responseMessage: String
  publicUrl;
  sortBy: String;
  categories = [];
  categoryId;


  constructor(
    private http: HttpClient,
    private router: Router,
    private alertManager: AlertManager,
    private darkMode : DarkModeService
  ) { }

  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.publicUrl = environment.publicUrl
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };

    this.http.get(environment.apiBaseUrl + '/category', this.httpOptions)
      .subscribe(response => {
        this.categories = response['categories'];
      }, error => {}
    );

    this.http.get(environment.apiBaseUrl + '/ad', this.httpOptions)
      .subscribe(response => {
        this.ads = response['ads'];
      }, error => {}
    );
  }

  ionViewWillEnter() {
    this.http.get(environment.apiBaseUrl + '/ad', this.httpOptions)
      .subscribe(response => {
        this.ads = response['ads'];
      }, error => {}
    );
  }

  cancelSearch() {
    this.searchText = null;
    this.filter();
  }

  details(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;

    localStorage.setItem('ad', value);
    this.router.navigate(['/ad-details']);
  }

  filter() {

    let sort = {
      key: null,
      order: null
    };

    if (this.sortBy !== undefined) {
      if (this.sortBy !== "") {
        let values = this.sortBy.split('-');
        sort.key = values[0];
        sort.order = values[1];
      }
    }

    this.http.post(environment.apiBaseUrl + "/ads/filter", {
      category_id: this.categoryId,
      sort: sort.key !== null ? sort : null,
      search: this.searchText,
    }, this.httpOptions).subscribe(response => {
      this.ads = response['ads'];
    }, error => {
      this.ionViewWillEnter();
    });
  }

}
