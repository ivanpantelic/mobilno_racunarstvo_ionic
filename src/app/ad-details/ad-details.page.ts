import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertManager } from '../helpers/alert';
import {environment} from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-ad-details',
  templateUrl: './ad-details.page.html',
  styleUrls: ['./ad-details.page.scss'],
})
export class AdDetailsPage implements OnInit {

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode : DarkModeService
  ) { }

  ad;
  httpOptions;
  quantity;
  isOrderCreated: boolean
  publicUrl;

  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.publicUrl = environment.publicUrl
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
  }

  ionViewWillEnter() {
    this.http.get(environment.apiBaseUrl + '/ad/' + localStorage.getItem('ad'), this.httpOptions)
        .subscribe( response => {
          console.log(response);
          this.ad = response['ad'];
          localStorage.setItem('user_id', response['ad']['0'].user_id);
        }, error => {
          console.log(error);
    });
  }

  order()
  {
    
    this.isOrderCreated = true;
    this.http.post(environment.apiBaseUrl + '/temp_order_items',  {
      ad_id: this.ad[0].id,
      title: this.ad[0].title,
      quantity: this.quantity,
      price: this.ad[0].price
    }, this.httpOptions)
        .subscribe( response => {
          console.log(response);
          this.alertManager.showAlert("Success!", response['message'], 'tabs/splash-screen')
          this.isOrderCreated = false;
        }, error => {
          this.isOrderCreated = false;
          console.log(error);
          if(error.status == 422) {
            this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
          } else {
            this.alertManager.showAlert("Error!", error['error'].message, "tabs/splash-screen")
          }
    });
  }
}
