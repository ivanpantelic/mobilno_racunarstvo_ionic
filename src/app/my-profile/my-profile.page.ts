import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertManager } from '../helpers/alert';
import {environment} from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  name  = " "
  email = " "
  mobile_phone = " "
  img_url;
  publicUrl;
  httpOptions;
  isLogOut: boolean
  buttonClicked: boolean = false;
  isChanged: boolean
  currentPassword;
  newPassword;
  confirmPassword;
  dark: boolean;
  mode: string;

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode : DarkModeService
  ) { }

  ngOnInit() {
    this.mode = localStorage.getItem("mode");
    this.dark = this.mode == 'dark';

    this.darkMode.setDarkModeAllPages();

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };

    this.http.get(environment.apiBaseUrl + "/profile", this.httpOptions).subscribe( response => {
      this.name = response['user'].name
      this.email = response['user'].email
      this.mobile_phone = response['user'].mobile_phone
      this.img_url = response['user'].img_url
      this.publicUrl = environment.publicUrl
    }, error => {
      this.alertManager.showAlert("Error!", error.statusText, "")
    })
  }

  logout() {
    this.isLogOut = true;
    this.http.post(environment.apiBaseUrl + "/logout",null, this.httpOptions).subscribe( response => {
      this.alertManager.showAlert("Success!", response['message'], 'home')
    }, error => {
      this.isLogOut = false;
      this.alertManager.showAlert("Error!", error.statusText, "")
    })
  }

  showChangePasswordForm(){
    this.buttonClicked = true;
  }

  hideChangePasswordForm() {
    this.buttonClicked = false;
  }

  changePassword() {
    this.isChanged = true;
    const {currentPassword, newPassword, confirmPassword} = this
    const url = environment.apiBaseUrl + "/change"

    this.http.post(url, {
      currentPassword: currentPassword,
      newPassword: newPassword,
      confirmPassword: confirmPassword,

    },this.httpOptions).subscribe(response => {
      console.log(response);
      this.alertManager.showAlert("Success!", response['message'], 'tabs/my-profile')
      this.buttonClicked = false;
      this.isChanged = false;
      this.currentPassword = "";
      this.newPassword = "";
      this.confirmPassword = "";
    }, error => {
      console.log(error);
      this.isChanged = false;
      if(error.status == 422) {
        this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
      } else {
        this.alertManager.showAlert("Error!", error['error'].message, "")
      }
    });
  }

  updateDarkMode() {
    document.body.classList.toggle('dark', this.dark);
    if(this.dark == true) {
      this.mode = "dark";
    } else {
      this.mode = "light";
    }
    localStorage.setItem("mode", this.mode);
  }
  
}
