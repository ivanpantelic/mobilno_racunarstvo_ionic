import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertManager } from '../helpers/alert';
import { environment } from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
@Injectable()
export class LoginPage implements OnInit {

  get page() {
    return {
      title: 'Settings'
    };
  }
  
  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode: DarkModeService
  ) {}

  email: string = ""
  password: string = ""
  isBusy: boolean

  ngOnInit() {
   this.darkMode.setDarkModeAllPages();
  }

  async login() {
    this.isBusy = true;
    const { email, password } = this
    const url = environment.apiBaseUrl + "/login"

    this.http.post(url, {
      email: email,
      password: password,

    }).subscribe(response => {
      localStorage.setItem('token', response['0'].access_token);
      localStorage.setItem('customerId',response['customer_id'])
      this.alertManager.showAlert("Success!", response['message'], 'tabs')
    }, error => {
      this.isBusy = false;
      if (error.status == 422) {
        this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
      } else {
        this.alertManager.showAlert("Error!", error['error'].message, "")
      }
    });
  }




}
