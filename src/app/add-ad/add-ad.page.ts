import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AlertManager } from '../helpers/alert';
import { environment } from 'src/environments/environment';
import { PhotoService } from '../services/photo.service';
import { DarkModeService } from '../services/dark-mode.service';

@Component({
  selector: 'app-add-ad',
  templateUrl: './add-ad.page.html',
  styleUrls: ['./add-ad.page.scss'],
})

@Injectable()
export class AddAdPage implements OnInit {

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private photoService: PhotoService,
    private darkMode: DarkModeService
  ) { }
  
  httpOptions
  title: string = ""
  description: string = ""
  price: number
  quantity: number
  img_url: string
  isDisabled: boolean
  categoryId: number
  categories = [];

  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };

    this.http.get(environment.apiBaseUrl + '/category', this.httpOptions)
      .subscribe(response => {
        this.categories = response['categories'];
      }, error => {}
    );
  }

  create() {
    this.isDisabled = true;
    const { title, description, price, quantity } = this

    this.http.post(environment.apiBaseUrl + "/ad", {
      title: title,
      description: description,
      price: price,
      quantity: quantity,
      category_id: this.categoryId,
      img_url: this.img_url
    }, this.httpOptions).subscribe(response => {
      this.alertManager.showAlert("Success!", response['message'], 'tabs/splash-screen')
      this.isDisabled = false;
    }, error => {
      this.isDisabled = false;
      console.log(error);
      if (error.status == 401) {
        this.alertManager.showAlert("Error!", error.statusText, "login")
      } else {
        this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
      }
    });
  }

  fileChanged(event) {
    const files = event.target.files
    const data = new FormData()
    data.append('photo', files[0])

    this.http.post(environment.apiBaseUrl + "/ad-image", data)
      .subscribe(event => {
        this.img_url = event['img_url']
        this.alertManager.showAlert("Success!", "File uploaded succesfully", "");
      }, error => {
        this.alertManager.showAlert("Error!", error.error.message, "")
      })
  }

  async uploadPhoto() {
    let base64 = await this.photoService.addNewToGallery();
    if(base64 === undefined) return;
    this.http.post(environment.apiBaseUrl + "/upload-picture", {
      code: base64,
      context: 'ads'
    }, this.httpOptions).subscribe(response => {
      this.img_url = response['img_url']
      this.alertManager.showAlert("Success!", response['message'], '')
    }, error => {
      this.alertManager.showAlert("Error!", error.error.message, "")
    });
  }

}
