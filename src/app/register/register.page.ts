import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AlertManager } from '../helpers/alert';
import {environment} from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
@Injectable()
export class RegisterPage implements OnInit {

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode : DarkModeService,
    private photoService: PhotoService
    ) { }

  httpOptions
  name: string = ""
  email: string = ""
  password: string = ""
  mobile_phone: string = ""
  account_number: string = ""
  img_url: string = ""
  isRegistred: boolean

  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
  }

  register() {
    this.isRegistred = true;
    const {name, email, password, mobile_phone} = this
    this.http.post(environment.apiBaseUrl + "/register", {
      name: name,
      email: email,
      password: password,
      mobile_phone: mobile_phone,
      img_url: this.img_url,
      account_number: this.account_number,
    }).subscribe(response => {
      this.alertManager.showAlert("Success!", response['message'], "login")
    }, error => {
      this.isRegistred = false;
      this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
    });
  }

  fileChanged(event) {
    const files = event.target.files
    const data = new FormData()
    data.append('photo', files[0])

    this.http.post(environment.apiBaseUrl + "/profile-image", data)
      .subscribe(event => {
        this.img_url = event['img_url']
        this.alertManager.showAlert("Success!", "File uploaded succesfully", "");
      })
  }

  async uploadPhoto() {
    let base64 = await this.photoService.addNewToGallery();
    this.http.post(environment.apiBaseUrl + "/upload-picture", {
      code: base64,
      context: 'users'
    }, this.httpOptions).subscribe(response => {
      this.img_url = response['img_url']
      this.alertManager.showAlert("Success!", response['message'], '')
    }, error => {
      this.alertManager.showAlert("Error!", error.error.message, "")
    });
  }
}
