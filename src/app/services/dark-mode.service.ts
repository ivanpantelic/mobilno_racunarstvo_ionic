import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DarkModeService implements OnInit {

  public dark = false;
  public light = false;
  public mode: string;

  get page() {
    return {
      title: 'Settings'
    };
  }
  constructor() {
  }

  ngOnInit(): void {
  }

  public setDarkMode() {
    const prefersColor = window.matchMedia('(prefers-color-scheme: light)');
    this.dark = prefersColor.matches;
    this.updateDarkMode();

    prefersColor.addEventListener(
      'change',
      mediaQuery => {
        this.dark = mediaQuery.matches;
        if(this.dark == true) {
          this.mode = "dark";
        } else {
          this.mode = "light";
        }
        this.updateDarkMode();
      }
    );
  }

  public setDarkModeAllPages() {
    this.mode = localStorage.getItem("mode");
    if (this.mode === "dark") {
      const prefersColor = window.matchMedia('(prefers-color-scheme: dark)');
      this.dark = prefersColor.matches;
      this.updateDarkMode();

      prefersColor.addEventListener(
        'change',
        mediaQuery => {
          this.dark = mediaQuery.matches;
          this.updateDarkMode();
        }
      );
    } else if (this.mode === "light") {
      const prefersColor = window.matchMedia('(prefers-color-scheme: light)');
      this.light = prefersColor.matches;
      this.updateLightkMode();

      prefersColor.addEventListener(
        'change',
        mediaQuery => {
          this.light = mediaQuery.matches;
          this.updateLightkMode();
        }
      );
    }
  }
  
  public updateDarkMode() {
    document.body.classList.toggle('dark', this.dark);
    if(this.dark == true) {
      this.mode = "dark";
    } else {
      this.mode = "light";
    }
    localStorage.setItem("mode", this.mode);
  }

  updateLightkMode() {
    document.body.classList.toggle('light', this.light);
    if(this.dark == true) {
      this.mode = "dark";
    } else {
      this.mode = "light";
    }
    localStorage.setItem("mode", this.mode);
  }

}



