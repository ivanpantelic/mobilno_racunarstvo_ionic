import { Injectable } from '@angular/core';
import { Plugins, CameraResultType, Capacitor, FilesystemDirectory, 
  CameraPhoto, CameraSource, CameraOptions } from '@capacitor/core';
import { defineCustomElements } from '@ionic/pwa-elements/loader';
const {Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class PhotoService {

  public photos: Photo[] = [];
  public photo: Photo;

  constructor() { }

  public async addNewToGallery() {

    try {
      const capturedPhoto = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
        correctOrientation: true,
        quality: 100,
        saveToGallery: true
      });

      let base64 = await this.readAsBase64(capturedPhoto);
      return base64;
    } catch (error) {
      console.log(error);
    }
  }

   private async readAsBase64(cameraPhoto: CameraPhoto) {
    const response = await fetch(cameraPhoto.webPath!);
    const blob = await response.blob();
    return await this.convertBlobToBase64(blob) as string;  
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });
}

defineCustomElements(window);

interface Photo {
  filepath: string;
  webviewPath: string;
  base64?: string;
}


