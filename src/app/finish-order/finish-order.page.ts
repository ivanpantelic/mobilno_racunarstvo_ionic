import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertManager } from '../helpers/alert';
import { environment } from 'src/environments/environment';
import { DarkModeService } from '../services/dark-mode.service';
import { Router } from '@angular/router';

declare var Stripe: any;

@Component({
  selector: 'app-finish-order',
  templateUrl: './finish-order.page.html',
  styleUrls: ['./finish-order.page.scss'],
})
export class FinishOrderPage implements OnInit {


  paymentForm;

  constructor(
    private http: HttpClient,
    private alertManager: AlertManager,
    private darkMode: DarkModeService,
    private router: Router
  ) { }

  httpOptions;
  total_amount;
  address;
  note;
  isOrdered: boolean
  takePaymentResult: string;
  customer_id: string;
  user: any;
  stringifiedData: any; 
  data: any;
  

  ngOnInit() {
    this.darkMode.setDarkModeAllPages();
    this.total_amount = localStorage.getItem('total_amount');
    this.customer_id = localStorage.getItem('customerId');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
    
    this.getUser();
  }

  order() {
    this.isOrdered = true;
    this.http.post(environment.apiBaseUrl + '/order', {
      address: this.address,
      note: this.note
    }, this.httpOptions)
      .subscribe(response => {
        this.isOrdered = false;
        console.log(response);
        this.alertManager.showAlert("Success!", response['message'], "")
        if(response["status"]==true) {
          this.proceedToPay(response["order_id"]);
        }
      }, error => {
        this.isOrdered = false;
        console.log(error);
        if (error.status == 422) {
          this.alertManager.showAlert("Error!", Object.values(error['error'])[0][0], "")
        } else {
          this.alertManager.showAlert("Error!", error['error'].message, "")
        }
      });
  }

  proceedToPay(orderId: string) {
    // this.data = {
    //     primalac: "Ivan Pantelic",
    //     platilac: this.user["name"],
    //     iznos: this.total_amount + ' USD',
    //     sifraPlacanja: 289,
    //     svrhaUplate: "Transakcije po nalogu gradjana",
    //     uKoristRacuna: "1200000000",
    //     modelPoziv: ""
    // }
    // this.stringifiedData = JSON.stringify(this.data);

    this.http.post(environment.apiBaseUrl + '/generate', {
      name: this.user.customer_id,
      address: this.address,
      note: this.note,
      total_amount: this.total_amount,
      order_id: orderId
    }).subscribe(response => {
      localStorage.setItem('qrURL', response['url']);
      localStorage.setItem('transactionId', response['transaction_id']);
      localStorage.setItem('orderId', response['order_id']);
      console.log(response);
      this.router.navigate(['/instant-payment']);
    }, error => {
      console.log(error);
    });
  }

  getUser() {
    this.http.get(environment.apiBaseUrl + "/profile", this.httpOptions).subscribe( response => {
      this.user = response['user'];
    }, error => {
      this.alertManager.showAlert("Error!", error.statusText, "")
    })
  }

}
