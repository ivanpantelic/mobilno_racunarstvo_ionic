import { Component, OnInit} from '@angular/core';
import { DarkModeService } from '../services/dark-mode.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  publicUrl;

  dark = false;
  mode = 'light';


  get page() {
    return {
      title: 'Settings'
    };
  }
  constructor(
    private darkMode : DarkModeService
    ) {
  }
  ngOnInit() {
    this.publicUrl = environment.publicUrl;
    let mode = localStorage.getItem("mode");
    this.dark = mode == 'dark';
  }

  updateDarkMode() {
    document.body.classList.toggle('dark', this.dark);
    if(this.dark == true) {
      this.mode = "dark";
    } else {
      this.mode = "light";
    }
    localStorage.setItem("mode", this.mode);
  }

}
