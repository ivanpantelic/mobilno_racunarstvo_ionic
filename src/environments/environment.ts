// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiBaseUrl: 'http://nmobilno.develop/api/v1',
  // baseUrl: 'http://nmobilno.develop',
  // publicUrl: 'http://nmobilno.develop/uploads'
  apiBaseUrl: 'http://127.0.0.1:9999/api/v1',
  baseUrl: 'http://127.0.0.1:9999',
  publicUrl: 'http://127.0.0.1:9999/uploads'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
